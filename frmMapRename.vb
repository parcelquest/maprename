Option Strict Off
Option Explicit On
Friend Class frmMapRename
	Inherits System.Windows.Forms.Form
#Region "Windows Form Designer generated code "
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'For the start-up form, the first instance created is the default instance.
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtIndexFile As System.Windows.Forms.TextBox
	Public WithEvents cmdTriDoc As System.Windows.Forms.Button
	Public WithEvents lstPages As System.Windows.Forms.ListBox
	Public WithEvents lstCounty As System.Windows.Forms.ComboBox
	Public WithEvents cmdDone As System.Windows.Forms.Button
	Public WithEvents cmdRename As System.Windows.Forms.Button
	Public WithEvents txtOutputFolder As System.Windows.Forms.TextBox
	Public WithEvents txtInputFolder As System.Windows.Forms.TextBox
	Public WithEvents lblCount As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
      Me.components = New System.ComponentModel.Container()
      Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMapRename))
      Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
      Me.txtIndexFile = New System.Windows.Forms.TextBox()
      Me.txtOutputFolder = New System.Windows.Forms.TextBox()
      Me.txtInputFolder = New System.Windows.Forms.TextBox()
      Me.cmdTriDoc = New System.Windows.Forms.Button()
      Me.lstPages = New System.Windows.Forms.ListBox()
      Me.lstCounty = New System.Windows.Forms.ComboBox()
      Me.cmdDone = New System.Windows.Forms.Button()
      Me.cmdRename = New System.Windows.Forms.Button()
      Me.lblCount = New System.Windows.Forms.Label()
      Me.Label4 = New System.Windows.Forms.Label()
      Me.Label3 = New System.Windows.Forms.Label()
      Me.Label2 = New System.Windows.Forms.Label()
      Me.Label1 = New System.Windows.Forms.Label()
      Me.SuspendLayout()
      '
      'txtIndexFile
      '
      Me.txtIndexFile.AcceptsReturn = True
      Me.txtIndexFile.BackColor = System.Drawing.SystemColors.Window
      Me.txtIndexFile.Cursor = System.Windows.Forms.Cursors.IBeam
      Me.txtIndexFile.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.txtIndexFile.ForeColor = System.Drawing.SystemColors.WindowText
      Me.txtIndexFile.Location = New System.Drawing.Point(112, 184)
      Me.txtIndexFile.MaxLength = 0
      Me.txtIndexFile.Name = "txtIndexFile"
      Me.txtIndexFile.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.txtIndexFile.Size = New System.Drawing.Size(329, 25)
      Me.txtIndexFile.TabIndex = 10
      Me.ToolTip1.SetToolTip(Me.txtIndexFile, "Enter output folder or Click Browse button to select")
      '
      'txtOutputFolder
      '
      Me.txtOutputFolder.AcceptsReturn = True
      Me.txtOutputFolder.BackColor = System.Drawing.SystemColors.Window
      Me.txtOutputFolder.Cursor = System.Windows.Forms.Cursors.IBeam
      Me.txtOutputFolder.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.txtOutputFolder.ForeColor = System.Drawing.SystemColors.WindowText
      Me.txtOutputFolder.Location = New System.Drawing.Point(112, 136)
      Me.txtOutputFolder.MaxLength = 0
      Me.txtOutputFolder.Name = "txtOutputFolder"
      Me.txtOutputFolder.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.txtOutputFolder.Size = New System.Drawing.Size(329, 25)
      Me.txtOutputFolder.TabIndex = 3
      Me.ToolTip1.SetToolTip(Me.txtOutputFolder, "Enter output folder or Click Browse button to select")
      '
      'txtInputFolder
      '
      Me.txtInputFolder.AcceptsReturn = True
      Me.txtInputFolder.BackColor = System.Drawing.SystemColors.Window
      Me.txtInputFolder.Cursor = System.Windows.Forms.Cursors.IBeam
      Me.txtInputFolder.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.txtInputFolder.ForeColor = System.Drawing.SystemColors.WindowText
      Me.txtInputFolder.Location = New System.Drawing.Point(112, 88)
      Me.txtInputFolder.MaxLength = 0
      Me.txtInputFolder.Name = "txtInputFolder"
      Me.txtInputFolder.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.txtInputFolder.Size = New System.Drawing.Size(329, 25)
      Me.txtInputFolder.TabIndex = 2
      Me.ToolTip1.SetToolTip(Me.txtInputFolder, "Enter input folder or Click Browse button to select")
      '
      'cmdTriDoc
      '
      Me.cmdTriDoc.BackColor = System.Drawing.SystemColors.Control
      Me.cmdTriDoc.Cursor = System.Windows.Forms.Cursors.Default
      Me.cmdTriDoc.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cmdTriDoc.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cmdTriDoc.Location = New System.Drawing.Point(480, 136)
      Me.cmdTriDoc.Name = "cmdTriDoc"
      Me.cmdTriDoc.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cmdTriDoc.Size = New System.Drawing.Size(89, 25)
      Me.cmdTriDoc.TabIndex = 9
      Me.cmdTriDoc.Text = "Rename &Docs"
      Me.cmdTriDoc.UseVisualStyleBackColor = False
      '
      'lstPages
      '
      Me.lstPages.BackColor = System.Drawing.SystemColors.Window
      Me.lstPages.Cursor = System.Windows.Forms.Cursors.Default
      Me.lstPages.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.lstPages.ForeColor = System.Drawing.SystemColors.WindowText
      Me.lstPages.ItemHeight = 14
      Me.lstPages.Location = New System.Drawing.Point(512, 16)
      Me.lstPages.Name = "lstPages"
      Me.lstPages.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.lstPages.Size = New System.Drawing.Size(49, 18)
      Me.lstPages.Sorted = True
      Me.lstPages.TabIndex = 8
      Me.lstPages.Visible = False
      '
      'lstCounty
      '
      Me.lstCounty.BackColor = System.Drawing.SystemColors.Window
      Me.lstCounty.Cursor = System.Windows.Forms.Cursors.Default
      Me.lstCounty.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.lstCounty.ForeColor = System.Drawing.SystemColors.WindowText
      Me.lstCounty.Items.AddRange(New Object() {"MRN", "SBX", "SLO", "TRI", "TUO"})
      Me.lstCounty.Location = New System.Drawing.Point(112, 24)
      Me.lstCounty.Name = "lstCounty"
      Me.lstCounty.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.lstCounty.Size = New System.Drawing.Size(65, 27)
      Me.lstCounty.TabIndex = 7
      '
      'cmdDone
      '
      Me.cmdDone.BackColor = System.Drawing.SystemColors.Control
      Me.cmdDone.Cursor = System.Windows.Forms.Cursors.Default
      Me.cmdDone.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cmdDone.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cmdDone.Location = New System.Drawing.Point(480, 184)
      Me.cmdDone.Name = "cmdDone"
      Me.cmdDone.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cmdDone.Size = New System.Drawing.Size(89, 25)
      Me.cmdDone.TabIndex = 5
      Me.cmdDone.Text = "&E&xit"
      Me.cmdDone.UseVisualStyleBackColor = False
      '
      'cmdRename
      '
      Me.cmdRename.BackColor = System.Drawing.SystemColors.Control
      Me.cmdRename.Cursor = System.Windows.Forms.Cursors.Default
      Me.cmdRename.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cmdRename.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cmdRename.Location = New System.Drawing.Point(480, 88)
      Me.cmdRename.Name = "cmdRename"
      Me.cmdRename.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cmdRename.Size = New System.Drawing.Size(89, 25)
      Me.cmdRename.TabIndex = 4
      Me.cmdRename.Text = "Rename &Maps"
      Me.cmdRename.UseVisualStyleBackColor = False
      '
      'lblCount
      '
      Me.lblCount.BackColor = System.Drawing.SystemColors.Control
      Me.lblCount.Cursor = System.Windows.Forms.Cursors.Default
      Me.lblCount.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.lblCount.ForeColor = System.Drawing.Color.Red
      Me.lblCount.Location = New System.Drawing.Point(256, 24)
      Me.lblCount.Name = "lblCount"
      Me.lblCount.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.lblCount.Size = New System.Drawing.Size(185, 33)
      Me.lblCount.TabIndex = 12
      Me.lblCount.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'Label4
      '
      Me.Label4.BackColor = System.Drawing.SystemColors.Control
      Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
      Me.Label4.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
      Me.Label4.Location = New System.Drawing.Point(24, 192)
      Me.Label4.Name = "Label4"
      Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Label4.Size = New System.Drawing.Size(81, 17)
      Me.Label4.TabIndex = 11
      Me.Label4.Text = "Index File:"
      Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'Label3
      '
      Me.Label3.BackColor = System.Drawing.SystemColors.Control
      Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
      Me.Label3.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
      Me.Label3.Location = New System.Drawing.Point(56, 32)
      Me.Label3.Name = "Label3"
      Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Label3.Size = New System.Drawing.Size(49, 17)
      Me.Label3.TabIndex = 6
      Me.Label3.Text = "County:"
      Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'Label2
      '
      Me.Label2.BackColor = System.Drawing.SystemColors.Control
      Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
      Me.Label2.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
      Me.Label2.Location = New System.Drawing.Point(24, 144)
      Me.Label2.Name = "Label2"
      Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Label2.Size = New System.Drawing.Size(81, 17)
      Me.Label2.TabIndex = 1
      Me.Label2.Text = "Output Folder:"
      Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'Label1
      '
      Me.Label1.BackColor = System.Drawing.SystemColors.Control
      Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
      Me.Label1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
      Me.Label1.Location = New System.Drawing.Point(24, 96)
      Me.Label1.Name = "Label1"
      Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Label1.Size = New System.Drawing.Size(81, 17)
      Me.Label1.TabIndex = 0
      Me.Label1.Text = "Input Folder:"
      Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'frmMapRename
      '
      Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
      Me.BackColor = System.Drawing.SystemColors.Control
      Me.ClientSize = New System.Drawing.Size(601, 280)
      Me.Controls.Add(Me.txtIndexFile)
      Me.Controls.Add(Me.cmdTriDoc)
      Me.Controls.Add(Me.lstPages)
      Me.Controls.Add(Me.lstCounty)
      Me.Controls.Add(Me.cmdDone)
      Me.Controls.Add(Me.cmdRename)
      Me.Controls.Add(Me.txtOutputFolder)
      Me.Controls.Add(Me.txtInputFolder)
      Me.Controls.Add(Me.lblCount)
      Me.Controls.Add(Me.Label4)
      Me.Controls.Add(Me.Label3)
      Me.Controls.Add(Me.Label2)
      Me.Controls.Add(Me.Label1)
      Me.Cursor = System.Windows.Forms.Cursors.Default
      Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
      Me.Location = New System.Drawing.Point(4, 23)
      Me.Name = "frmMapRename"
      Me.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Text = "Rename Assessor Maps"
      Me.ResumeLayout(False)

   End Sub
#End Region 
#Region "Upgrade Support "
	Private Shared m_vb6FormDefInstance As frmMapRename
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmMapRename
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmMapRename()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	
	Private Sub cmdDone_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdDone.Click
		Me.Close()
	End Sub
	
	Private Sub cmdRename_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdRename.Click
		Dim iRet As Short
		
		On Error GoTo cmdRename_Error
		
		g_sCnty = lstCounty.Text
		g_sInputDir = txtInputFolder.Text & "\" & g_sCnty
		g_sOutputDir = txtOutputFolder.Text & "\" & g_sCnty
      If Dir(g_sOutputDir, FileAttribute.Directory) = "" Then
         MkDir(g_sOutputDir)
      End If
		
		cmdRename.Enabled = False
		cmdTriDoc.Enabled = False
		Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
		Call RenameCntyMaps(g_sCnty)
		Me.Cursor = System.Windows.Forms.Cursors.Default
		cmdRename.Enabled = True
		cmdTriDoc.Enabled = True
		Exit Sub
		
cmdRename_Error: 
		MsgBox("Error creating folder: " & g_sOutputDir & " (" & Err.Description & ")")
		cmdRename.Enabled = True
	End Sub
	
	Private Sub cmdTriDoc_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdTriDoc.Click
		Dim iRet As Short
		
		On Error GoTo cmdRename_Error
		
		g_sInputDir = txtInputFolder.Text & "\"
		g_sOutputDir = txtOutputFolder.Text & "\"
		g_sIndexFile = txtIndexFile.Text
		
		cmdRename.Enabled = False
		cmdTriDoc.Enabled = False
		Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
		Call RenameCntyDocs(g_sCnty)
		Me.Cursor = System.Windows.Forms.Cursors.Default
		cmdRename.Enabled = True
		cmdTriDoc.Enabled = True
		
		Exit Sub
		
cmdRename_Error: 
		MsgBox("Error renaming TriDocs: " & Err.Description)
		cmdRename.Enabled = True
		cmdTriDoc.Enabled = True
	End Sub
	
	Private Sub frmMapRename_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		txtInputFolder.Text = g_sInputDir
		txtOutputFolder.Text = g_sOutputDir
   End Sub
	
   Private Sub lstCounty_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstCounty.SelectedIndexChanged
      g_sCnty = lstCounty.Text
      Call InitCounty(g_sCnty)

      txtInputFolder.Text = g_sInputDir
      txtOutputFolder.Text = g_sOutputDir
   End Sub
End Class
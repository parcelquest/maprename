﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports System.IO

Module modTUO

   'Copy document from image number to doc number
   '   Private Function renOneDoc(ByRef sImgNum As String, ByRef sDocNum As String) As Integer
   '      Dim dstFile, srcFile, sTmp As String
   '      Dim sDoc, sBook, sPage, sExt As String
   '      Dim iTmp, iCnt As Short
   '      Dim dstPath As String

   '      'Check output folder
   '      sBook = Left(sDocNum, 4)
   '      sPage = Mid(sDocNum, 5, 3)
   '      sTmp = g_sOutputDir & "\" & sBook
   '      If Not Directory.Exists(sTmp) Then
   '         MkDir(sTmp)
   '      End If
   '      sTmp = g_sOutputDir & "\" & sBook & "\" & sPage
   '      If Not Directory.Exists(sTmp) Then
   '         MkDir(sTmp)
   '      End If

   '      dstPath = sTmp & "\"

   '      'Copy files
   '      srcFile = sImgNum & ".*"
   '      Dim fileEntries As String() = Directory.GetFiles(g_sInputDir, srcFile)
   '      Dim fileName As String
   '      iCnt = 0
   '      sDoc = sDocNum

   '      For Each fileName In fileEntries
   '         iTmp = InStr(2, fileName, ".")
   '         sExt = Mid(fileName, iTmp, 4)
   '         dstFile = sDoc & sExt

   '         Try
   '            My.Computer.FileSystem.CopyFile(fileName, dstPath & dstFile)
   '            iCnt = iCnt + 1
   '         Catch ex As Exception
   '            LogMsg("Error copying file " & srcFile & " TO " & dstFile)
   '            renOneDoc = -1
   '            GoTo renOneDoc_Exit
   '         End Try
   '      Next

   '      renOneDoc = iCnt

   'renOneDoc_Exit:
   '   End Function
   Private Function renOneDoc(ByRef sImgNum As String, ByRef sDocNum As String) As Integer
      Dim dstFile, srcFile As String
      Dim sDoc, sExt As String
      Dim iTmp, iCnt As Integer
      Dim dstPath As String

      'Check output folder
      dstPath = g_sOutputDir & "\"

      'Copy files
      srcFile = sImgNum & ".*"
      Dim fileEntries As String() = Directory.GetFiles(g_sInputDir, srcFile)
      Dim fileName As String
      iCnt = 0
      sDoc = sDocNum

      For Each fileName In fileEntries
         iTmp = InStr(2, fileName, ".")
         sExt = Mid(fileName, iTmp, 4)
         dstFile = sDoc & sExt

         Try
            LogMsg0("Copy " & fileName & " --> " & dstPath & dstFile)
            My.Computer.FileSystem.CopyFile(fileName, dstPath & dstFile, overwrite:=True)
            iCnt = iCnt + 1
            If g_bRemove Then
               My.Computer.FileSystem.DeleteFile(fileName)
            End If
         Catch ex As Exception
            LogMsg("***** Error copying file " & srcFile & " TO " & dstFile)
            renOneDoc = -1
            GoTo renOneDoc_Exit
         End Try
      Next

      renOneDoc = iCnt

renOneDoc_Exit:
   End Function

   Private Function renOneDir(ByRef sIndexFile As String) As Integer
      Dim iRet, iRowCnt, fhIdx As Integer
      Dim sBuf, sTmp, sImgNum, sDocNum As String
      Dim aStr() As String

      If File.Exists(sIndexFile) Then
         fhIdx = FreeFile()
         Try
            LogMsg("Open index file: " & sIndexFile)
            FileOpen(fhIdx, sIndexFile, OpenMode.Input, OpenAccess.Read, OpenShare.LockWrite)
         Catch ex As Exception
            LogMsg("***** ERROR: Cannot open file -->" & sIndexFile & " [" & ex.Message & "]")
            renOneDir = -2
            Exit Function
         End Try

         'Skip header
         sBuf = LineInput(fhIdx)

         sImgNum = ""
         iRowCnt = 0
         Do While Not EOF(fhIdx)
            sBuf = LineInput(fhIdx)
            If Right(sBuf, 5) <> "{EOR}" Then
               Do
                  sTmp = LineInput(fhIdx)
                  sBuf = sBuf + "," + sTmp
               Loop Until Right(sBuf, 5) = "{EOR}" Or EOF(fhIdx)
            End If

            iRowCnt = iRowCnt + 1
            aStr = sBuf.Split(vbTab)
            If aStr.Length > g_iDocNumIdx Then
               If sImgNum <> aStr(g_iImgNumIdx) Then
                  sImgNum = aStr(g_iImgNumIdx)
                  sDocNum = aStr(g_iDocNumIdx)
                  If sDocNum <> "" And sImgNum <> "" Then
                     iRet = renOneDoc(sImgNum, sDocNum)
                     If iRet > 0 Then
                        g_iMaps = g_iMaps + iRet
                     End If
                  Else
                     LogMsg("*** Missing DocNum or ImgNum at row: " & iRowCnt)
                  End If
               End If
            Else
               LogMsg("*** Bad input record: " & sBuf)
            End If

            System.Windows.Forms.Application.DoEvents()
         Loop

         FileClose(fhIdx)
         LogMsg(sIndexFile & " is completed: " & g_iMaps & " images")
         renOneDir = 0
      Else
         LogMsg("***** ERROR: File not file -->" & sIndexFile)
         renOneDir = -1
      End If
   End Function

   ' Process all files in the directory passed in, recurse on any directories  
   ' that are found, and process the files they contain. 
   Public Sub renameTuoDocs(ByVal targetDirectory As String)
      Dim fileName As String

      fileName = targetDirectory & "\TCROR.HFW"
      If File.Exists(fileName) Then
         g_sInputDir = targetDirectory
         renOneDir(fileName)

         'Check for second index
         fileName = targetDirectory & "\TCROR_1.HFW"
         If File.Exists(fileName) Then
            renOneDir(fileName)
         End If
      End If

      Dim subdirectoryEntries As String() = Directory.GetDirectories(targetDirectory)
      ' Recurse into subdirectories of this directory. 
      Dim subdirectory As String
      For Each subdirectory In subdirectoryEntries
         renameTuoDocs(subdirectory)
      Next subdirectory
   End Sub

End Module

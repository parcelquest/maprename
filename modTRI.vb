Option Strict Off
Option Explicit On
Imports System.IO

Module modTRI
   Private Function renOneDoc(ByRef sImgNum As String, ByRef sDocNum As String) As Integer
      Dim dstFile, srcFile As String
      Dim sImgName As String
      Dim dstPath As String

      'Check output folder
      dstPath = g_sOutputDir & "\"

      'Check imageNum - if number, add leading 0 to 8 digits
      If sImgNum.Chars(0) < "A" And sImgNum.Length < 8 Then
         sImgName = CInt(sImgNum).ToString("D8")
      Else
         sImgName = sImgNum
      End If

      'Copy files
      srcFile = g_sInputDir & "\" & sImgName & ".tif"
      dstFile = g_sOutputDir & "\" & sDocNum & ".tif"
      LogMsg0("Copy " & srcFile & " --> " & dstFile)
      Try
         My.Computer.FileSystem.CopyFile(srcFile, dstFile, overwrite:=True)
         If g_bRemove Then
            My.Computer.FileSystem.DeleteFile(srcFile)
         End If
      Catch ex As Exception
         LogMsg("***** Error copying file " & srcFile & " TO " & dstFile)
         renOneDoc = -1
         GoTo renOneDoc_Exit
      End Try

      renOneDoc = 1

renOneDoc_Exit:
   End Function

   Private Function renOneDir(ByRef sIndexFile As String) As Integer
      Dim iRet, iRowCnt, fhIdx As Integer
      Dim sBuf, sImgNum, sDocNum As String
      Dim aStr() As String

      If File.Exists(sIndexFile) Then
         fhIdx = FreeFile()
         Try
            LogMsg("Open index file: " & sIndexFile)
            FileOpen(fhIdx, sIndexFile, OpenMode.Input, OpenAccess.Read, OpenShare.LockWrite)
         Catch ex As Exception
            LogMsg("***** ERROR: Cannot open file -->" & sIndexFile & " [" & ex.Message & "]")
            renOneDir = -2
            Exit Function
         End Try

         'Skip header
         sBuf = LineInput(fhIdx)

         sImgNum = ""
         iRowCnt = 0
         Do While Not EOF(fhIdx)
            sBuf = RemoveCommaInQuote(LineInput(fhIdx))
            iRowCnt = iRowCnt + 1
            aStr = sBuf.Split(",")
            If aStr.Length > g_iImgNumIdx Then
               If sImgNum <> aStr(g_iImgNumIdx) Then
                  sImgNum = aStr(g_iImgNumIdx)
                  sDocNum = aStr(g_iDocNumIdx)
                  If sDocNum <> "" And sImgNum <> "" Then
                     iRet = renOneDoc(sImgNum, sDocNum)
                     If iRet > 0 Then
                        g_iMaps = g_iMaps + iRet
                     End If
                  Else
                     LogMsg("*** Missing DocNum or ImgNum at line: " & g_iMaps)
                  End If
               End If
            Else
               LogMsg("*** Bad input record: " & sBuf)
            End If
            System.Windows.Forms.Application.DoEvents()
         Loop

         FileClose(fhIdx)
         LogMsg(sIndexFile & " is completed: " & g_iMaps & " images")
         renOneDir = 0
      Else
         LogMsg("***** ERROR: File not file -->" & sIndexFile)
         renOneDir = -1
      End If
   End Function

   ' Process all files in the directory passed in, recurse on any directories  
   ' that are found, and process the files they contain. 
   Public Sub renameTriDocs(ByVal targetDirectory As String)
      Dim fileName, filePath As String

      filePath = targetDirectory & "\*orhit*.csv"
      fileName = Dir(filePath)
      While fileName <> ""
         g_sInputDir = targetDirectory
         renOneDir(g_sInputDir & "\" & fileName)
         fileName = Dir()
      End While

      Dim subdirectoryEntries As String() = Directory.GetDirectories(targetDirectory)
      ' Recurse into subdirectories of this directory. 
      Dim subdirectory As String
      For Each subdirectory In subdirectoryEntries
         renameTriDocs(subdirectory)
      Next subdirectory
   End Sub

   '   Public Sub renameTriDocs()
   '      Dim sInpath, sTmp As String
   '      Dim sOutpath, sBook As String
   '      Dim iCnt, iRet, iBook As Short
   '      Dim fd As Short

   '      On Error GoTo renameTriDocs_Error

   '      'Search all input folder
   '      g_iMaps = 0
   '      iBook = 100
   '      ReDim g_asBooks(iBook)

   '      sInpath = g_sInputDir & "\*.001"
   '      sTmp = Dir(sInpath, FileAttribute.Normal)
   '      iCnt = 0
   '      Do While sTmp <> ""
   '         g_asBooks(iCnt) = Left(sTmp, Len(sTmp) - 3)
   '         iCnt = iCnt + 1
   '         If iCnt >= iBook Then
   '            ReDim Preserve g_asBooks(iBook + 100)
   '            iBook = iBook + 100
   '         End If

   '         sTmp = Dir()
   '      Loop
   '      iBook = iCnt

   '      For iCnt = 0 To iBook - 1
   '         iRet = copyDocs(g_asBooks(iCnt))
   '         System.Windows.Forms.Application.DoEvents()
   '      Next

   '      LogMsg("TRI Docs rename is complete with " & g_iMaps & " images")
   '      Exit Sub

   'renameTriDocs_Error:
   '      LogMsg("Error in renameTriDocs: " & Err.Description)
   '   End Sub
End Module
Option Strict Off
Option Explicit On
Module CntyInfo
	
	Const MAX_CNTYS As Short = 62
	
	Structure CntyInfo
		Dim sCountyCode As String
      Dim iCountyID As Integer
      Dim iApnLen As Integer
      Dim iBookLen As Integer
      Dim iPageLen As Integer
      Dim iCmpLen As Integer
      Dim sCountyName As String
      Dim sApnFormat As String
      Dim sOtherFormat As String
      Dim sBook As String
   End Structure

   Structure APNPart
      Dim sOptPart As String
      Dim iPartLen As Integer
      Dim iOption As Integer '1=req, 0=optional, 16=special case
   End Structure

   Structure SpecialAPN
      Dim sApn As String
      Dim iPageLen As Integer
   End Structure
   Public Const MAX_LAKESPEC As Integer = 9
   Public g_aLakeSpec(MAX_LAKESPEC) As SpecialAPN

   Public atCountyInfo(MAX_CNTYS) As CntyInfo
   Public g_iCounties As Integer
   Public g_iCntyIdx As Integer
   Public g_aApnParts() As APNPart
   Public g_iApnParts As Integer
   Public g_iApnLen As Integer
	
	Public Sub initLake()
		g_aLakeSpec(0).sApn = "00603399"
		g_aLakeSpec(0).iPageLen = 3
		g_aLakeSpec(1).sApn = "00655999"
		g_aLakeSpec(1).iPageLen = 2
		g_aLakeSpec(2).sApn = "00806599"
		g_aLakeSpec(2).iPageLen = 3
		g_aLakeSpec(3).sApn = "00873999"
		g_aLakeSpec(3).iPageLen = 2
		g_aLakeSpec(4).sApn = "01400699"
		g_aLakeSpec(4).iPageLen = 3
		g_aLakeSpec(5).sApn = "01450999"
		g_aLakeSpec(5).iPageLen = 2
		g_aLakeSpec(6).sApn = "02300599"
		g_aLakeSpec(6).iPageLen = 3
		g_aLakeSpec(7).sApn = "11410999"
		g_aLakeSpec(7).iPageLen = 2
		g_aLakeSpec(8).sApn = "11501899"
		g_aLakeSpec(8).iPageLen = 3
		g_aLakeSpec(9).sApn = "62837999"
		g_aLakeSpec(9).iPageLen = 2
	End Sub
	
	Public Sub initCntyInfo(ByRef sFilename As String)
		Dim aStr() As String
      Dim fh, iRet As Integer
		Dim strTmp As String
		
		On Error GoTo initCntyInfo_Error
		
		fh = FreeFile
		FileOpen(fh, sFilename, OpenMode.Input)
		strTmp = LineInput(fh) 'Skip header line
		
		g_iCounties = 0
		Do While Not EOF(fh)
			strTmp = LineInput(fh)
			If Left(strTmp, 1) <> ";" Then
				'CountyCode , CountyID, ApnLen, BookLen, PageLen, CmpLen, CountyName, ApnFormat, OtherFormat, Book
				g_iCounties = g_iCounties + 1
            aStr = strTmp.Split(",")
            If aStr.Length < 8 Then Exit Do

				atCountyInfo(g_iCounties).sCountyCode = aStr(0)
            atCountyInfo(g_iCounties).iCountyID = aStr(1)
            atCountyInfo(g_iCounties).iApnLen = aStr(2)
            atCountyInfo(g_iCounties).iBookLen = aStr(3)
            atCountyInfo(g_iCounties).iPageLen = aStr(4)
            atCountyInfo(g_iCounties).iCmpLen = aStr(5)
				atCountyInfo(g_iCounties).sCountyName = aStr(6)
				atCountyInfo(g_iCounties).sApnFormat = aStr(7)
				'atCountyInfo(g_iCounties).sOtherFormat = aStr(8)
				'atCountyInfo(g_iCounties).sBook = "" & aStr(9)
			End If
		Loop 
		FileClose(fh)
		
		'Uninitialize county index
		g_iCntyIdx = 9999
initCntyInfo_Error: 
	End Sub
	
	Public Function getCountyIdx(ByRef sCnty As String) As Short
		Dim iRet As Short
		
		For iRet = 1 To g_iCounties
			If atCountyInfo(iRet).sCountyCode = sCnty Then
				Exit For
			End If
		Next 
		If iRet > g_iCounties Then
			iRet = 0
		End If
		getCountyIdx = iRet
	End Function
	
	Public Function setCounty(ByRef sCnty As String) As Boolean
		Dim iRet As Short
		Dim aStr() As String
		
		If g_iCounties = 0 Then
			setCounty = False
			Exit Function
		End If
		
		If sCnty = "LAK" Then
			Call initLake()
		End If
		
		iRet = getCountyIdx(sCnty)
		If iRet > 0 Then
			g_iCntyIdx = iRet
			g_iApnLen = atCountyInfo(g_iCntyIdx).iApnLen
			
			'Parse APN format
         'iRet = ParseStr(atCountyInfo(iRet).sApnFormat, "-", aStr)
         aStr = atCountyInfo(iRet).sApnFormat.Split("-")
         g_iApnParts = aStr.Length
			
			ReDim g_aApnParts(g_iApnParts)
			For iRet = 0 To g_iApnParts - 1
				g_aApnParts(iRet).iPartLen = Len(aStr(iRet))
				If aStr(iRet) >= "X" Then
					g_aApnParts(iRet).iOption = 0
					g_aApnParts(iRet).sOptPart = New String(Chr(48), g_aApnParts(iRet).iPartLen)
				Else
					g_aApnParts(iRet).iOption = 1
				End If
			Next 
			setCounty = True
		Else
			setCounty = False
		End If
		
	End Function
	
	Public Function reFormatApn(ByRef sApn As String) As String
		Dim iRet, iPos As Short
		Dim sRet, sTmp As String
		Dim bDone As Boolean
		
		On Error GoTo reFormatApn_Error
		
		'This function only works after setCounty() is called
		If g_iCntyIdx = 0 Then
			reFormatApn = ""
			Exit Function
		End If
		
      bDone = False
      sRet = sApn

		'Special formatting
		Select Case atCountyInfo(g_iCntyIdx).sCountyCode
			Case "ORG"
				sTmp = Left(sApn, 3)
				If sTmp >= "930" And sTmp < "940" Then
					'Condo 3-2-3
					sRet = sTmp & "-" & Mid(sApn, 4, 2) & "-" & Mid(sApn, 6, 3)
					bDone = True
				End If
		End Select
		
		'Standard formating
		If bDone = False Then
			sTmp = Left(sApn, g_aApnParts(0).iPartLen)
			'Current format leaves space alone.
			'sRet = sTmp
			sRet = RTrim(sTmp)
			iPos = 1 + g_aApnParts(0).iPartLen
			For iRet = 1 To g_iApnParts - 1
				sTmp = Mid(sApn, iPos, g_aApnParts(iRet).iPartLen)
				If g_aApnParts(iRet).iOption = 0 Then
					'If optional token is blank or zero, skip it
					If sTmp <= g_aApnParts(iRet).sOptPart Then sTmp = ""
				End If
				
				If sTmp <> "" Then
					sRet = sRet & "-" & sTmp
				End If
				iPos = iPos + g_aApnParts(iRet).iPartLen
			Next 
		End If
		reFormatApn = sRet
		Exit Function
		
reFormatApn_Error: 
		reFormatApn = ""
	End Function
	
	'Output: \BK???\??????
	Public Function formatMapLink(ByRef sApn As String) As String
      Dim sBook, sRet, sTmp, sPage As String
		Dim bDone As Boolean
		Dim iRet, iPageLen As Short
		
		On Error GoTo formatMapLink_Error
		
		'This function only works after setCounty() is called
		If g_iCntyIdx = 0 Then
			formatMapLink = ""
			Exit Function
		End If
		
		bDone = False
		sBook = RTrim(Left(sApn, atCountyInfo(g_iCntyIdx).iBookLen))
		iPageLen = atCountyInfo(g_iCntyIdx).iPageLen
		
		'Special formatting
		Select Case atCountyInfo(g_iCntyIdx).sCountyCode
			Case "ALA"
				If sBook = "0000" Then
					sBook = "000"
				End If
				
			Case "FRE"
				If sBook >= "700" And sBook < "800" Then
					iPageLen = 3
				End If
				
			Case "LAK"
				iPageLen = 3
            For iRet = 0 To MAX_LAKESPEC
               If g_aLakeSpec(iRet).sApn > sApn Then Exit For
               iPageLen = g_aLakeSpec(iRet).iPageLen
            Next
				
			Case "LAS"
				If sBook = "999" Then
					iPageLen = 3
				End If
				
			Case "SBX"
				If sBook = "004" Or sBook = "128" Then
					iPageLen = 3
				End If
				
			Case "SDX"
				If sBook = "760" Then
					iPageLen = 3
				End If
				
		End Select
		
		'Standard formating
		sPage = Mid(sApn, atCountyInfo(g_iCntyIdx).iBookLen + 1, iPageLen)
		sRet = "\BK" & sBook & "\" & sBook & sPage
		
		formatMapLink = sRet
		Exit Function
		
formatMapLink_Error: 
		formatMapLink = ""
	End Function
	
	Public Function getCountyFormat(ByRef sCnty As String) As String
		Dim iRet As Short
		
		If sCnty = "" Then
			iRet = g_iCntyIdx
		Else
			For iRet = 1 To g_iCounties
				If atCountyInfo(iRet).sCountyCode = sCnty Then
					Exit For
				End If
			Next 
		End If
		
		If iRet > g_iCounties Then
			getCountyFormat = ""
		Else
			getCountyFormat = atCountyInfo(iRet).sApnFormat
		End If
	End Function
End Module
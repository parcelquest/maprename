Option Strict Off
Option Explicit On
Module modDocs
	
	Public Function copyDocs(ByRef sDoc As String) As Short
		Dim dstFile, srcFile, sTmp As String
      Dim sCurDir, sFile As String
      Dim sPage, sBook, sExt As String
      Dim iTmp, iBook, iPage, iCnt As Short
      Dim srcPath, dstPath As String

      On Error GoTo copyDocs_Error

      srcPath = g_sInputDir
      dstPath = g_sOutputDir
      If Right(g_sInputDir, 1) <> "\" Then srcPath = g_sInputDir & "\"
      If Right(g_sOutputDir, 1) <> "\" Then dstPath = g_sOutputDir & "\"

      'Check output folder
      sBook = Left(sDoc, 4)
      sPage = Mid(sDoc, 5, 3)
      sTmp = dstPath & sBook
      If Dir(sTmp, FileAttribute.Directory) = "" Then
         MkDir(sTmp)
      End If
      sTmp = dstPath & sBook & "\" & sPage
      If Dir(sTmp, FileAttribute.Directory) = "" Then
         MkDir(sTmp)
      End If

      dstPath = dstPath & sBook & "\" & sPage & "\"

      'Copy files
      srcFile = Dir(g_sInputDir & "\" & sDoc & "*")
      Do While srcFile <> ""
         iTmp = InStr(2, srcFile, ".")
         iPage = iPage + 1
         If iTmp > 1 Then
            sExt = Mid(srcFile, iTmp + 2, 2)
            dstFile = sDoc & sExt
         Else
            dstFile = sDoc & iPage
         End If

         FileCopy(srcPath & srcFile, dstPath & dstFile)
         g_iMaps = g_iMaps + 1
         srcFile = Dir()
      Loop

      copyDocs = 0
      Exit Function

copyDocs_Error:
      LogMsg("Error copying file " & srcFile & " TO " & dstFile)
      copyDocs = -1
   End Function

   'Copy document from image number to doc number
   Public Function renDocs(ByRef sImgNum As String, ByRef sDocNum As String) As Short
      Dim dstFile, srcFile, sTmp As String
      Dim sCurDir, sFile As String
      Dim sDoc, sBook, sPage, sExt As String
      Dim iTmp, iBook, iPage, iCnt As Short
      Dim srcPath, dstPath As String

      On Error GoTo renDocs_Error

      'Check output folder
      sBook = Left(sDocNum, 4)
      sPage = Mid(sDocNum, 5, 3)
      sTmp = g_sOutputDir & sBook
      If Dir(sTmp, FileAttribute.Directory) = "" Then
         MkDir(sTmp)
      End If
      sTmp = g_sOutputDir & sBook & "\" & sPage
      If Dir(sTmp, FileAttribute.Directory) = "" Then
         MkDir(sTmp)
      End If

      dstPath = g_sOutputDir & sBook & "\" & sPage & "\"

      'Copy files
      srcFile = Dir(g_sInputDir & "\" & sImgNum & ".*")
      iPage = 0
      sDoc = sDocNum & "."
      Do While srcFile <> ""
         iTmp = InStr(2, srcFile, ".")
         iPage = iPage + 1
         If iTmp > 1 Then
            sExt = Mid(srcFile, iTmp + 2, 2)
            dstFile = sDoc & sExt
         Else
            dstFile = sDoc & iPage
         End If

         FileCopy(g_sInputDir & srcFile, dstPath & dstFile)
         g_iMaps = g_iMaps + 1
         srcFile = Dir()
      Loop

      renDocs = 0
      Exit Function

renDocs_Error:
      LogMsg("Error copying file " & srcFile & " TO " & dstFile)
      renDocs = -1
   End Function
	
	Public Sub renameDocs_Csv(ByRef sCnty As String)
		Dim sImgNum, sInpath, sTmp, sDocNum As String
		Dim sBook, sOutpath, sBuf As String
		Dim iCnt, iRet, iBook As Short
		Dim fd As Short
		Dim lCnt As Integer
		Dim aStr() As String
		
		On Error GoTo renameDocs_Csv_Error
		
		'Search all input folder
		g_iMaps = 0
		
		If Right(g_sInputDir, 1) <> "\" Then g_sInputDir = g_sInputDir & "\"
		If Right(g_sOutputDir, 1) <> "\" Then g_sOutputDir = g_sOutputDir & "\"
		
		'Open index file
		fd = FreeFile
		FileOpen(fd, g_sIndexFile, OpenMode.Input)
		
		'Skip header
		sBuf = LineInput(fd)
		
		Do While Not EOF(fd)
			sBuf = LineInput(fd)
			g_iMaps = g_iMaps + 1
         'iRet = ParseStr(sBuf, ",", aStr)
         aStr = sBuf.Split(",")
			sImgNum = stripQuote(aStr(g_iImgNumIdx))
			sDocNum = stripQuote(aStr(g_iDocNumIdx))
			If sDocNum <> "" And sImgNum <> "" Then
				iRet = renDocs(sImgNum, sDocNum)
			Else
				LogMsg("Missing DocNum or ImgNum at line: " & g_iMaps)
			End If
			System.Windows.Forms.Application.DoEvents()
		Loop 
		
		FileClose(fd)
		LogMsg(sCnty & " Docs rename is complete with " & g_iMaps & " images")
		Exit Sub
		
renameDocs_Csv_Error: 
		LogMsg("Error in renameTriDocs: " & Err.Description)
	End Sub
End Module
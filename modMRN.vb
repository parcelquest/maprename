Option Strict Off
Option Explicit On
Imports System.IO

Module modMRN

   Public Sub renameMRN()
      Dim sInpath, sTmp As String
      Dim sOutpath, sBook As String
      Dim iCnt, iRet, iBook As Short

      'Process county index
      g_iMaps = 0
      sInpath = g_sInputDir & "\MapBookIndex"
      'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
      sTmp = Dir(sInpath, FileAttribute.Directory)
      If sTmp <> "" Then
         sTmp = g_sOutputDir & "\Bk000"
         If 0 = File.Exists(sTmp) Then
            MkDir(sTmp)
         End If
         sOutpath = g_sOutputDir & "\Bk000\00000.00"
         sTmp = sInpath & "\index.tif"
         FileCopy(sTmp, sOutpath)
         g_iMaps = g_iMaps + 1
      End If

      'Search all input folder
      iBook = 100
      ReDim g_asBooks(iBook)

      sInpath = g_sInputDir & "\Book*"
      'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
      sTmp = Dir(sInpath, FileAttribute.Directory)
      iCnt = 0
      Do While sTmp <> ""
         g_asBooks(iCnt) = sTmp
         iCnt = iCnt + 1
         If iCnt >= iBook Then
            ReDim Preserve g_asBooks(iBook + 100)
            iBook = iBook + 100
         End If

         'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
         sTmp = Dir()
      Loop
      iBook = iCnt

      For iCnt = 0 To iBook - 1
         sTmp = g_asBooks(iCnt)
         sBook = Mid(sTmp, 5)
         sOutpath = g_sOutputDir & "\Bk" & sBook
         If 0 = File.Exists(sOutpath) Then
            MkDir(sOutpath)
         End If
         sInpath = g_sInputDir & "\" & sTmp & "\"
         sOutpath = g_sOutputDir & "\BK" & sBook & "\"
         iRet = copyBook(sInpath, sOutpath, sBook)

         System.Windows.Forms.Application.DoEvents()
      Next

      LogMsg("MRN rename is complete with " & g_iMaps & " images")
      Exit Sub

renameMRN_Error:
      LogMsg("Error in renameMRN: " & Err.Description)
   End Sub

   Private Function copyBook(ByRef srcPath As String, ByRef dstpath As String, ByRef sBook As String) As Short
      Dim dstFile, srcFile, sTmp As String
      'UPGRADE_NOTE: curDir was upgraded to curDir_Renamed. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1061"'
      Dim curDir_Renamed, sFile As String
      Dim sPage As String
      Dim iTmp, iBook, iPage, iCnt As Short

      On Error GoTo copyBook_Error

      curDir_Renamed = "     "

      'Copy the rest
      srcFile = Dir(srcPath & "*.tif")
      Do While srcFile <> ""
         iTmp = InStr(2, srcFile, "-")
         If iTmp > 1 Then
            sPage = Mid(srcFile, iTmp + 1, 2)
            dstFile = sBook & sPage & ".00"
         ElseIf UCase(Left(srcFile, 5)) = "INDEX" Then
            iTmp = InStr(2, srcFile, ".")
            If iTmp > 6 Then
               'Pick up sheet number
               sTmp = Left(srcFile, iTmp - 1)
               iCnt = Val(Mid(sTmp, 6))
            Else
               iCnt = 0
            End If
            dstFile = sBook & "00." & iCnt.ToString("D2") 'VB6.Format(iCnt, "##00")
         Else
            dstFile = "Bad." & iCnt
         End If

         FileCopy(srcPath & srcFile, dstpath & dstFile)
         g_iMaps = g_iMaps + 1
         srcFile = Dir()
      Loop

      copyBook = 0
      Exit Function

copyBook_Error:
      LogMsg("Error copying file " & srcFile & " TO " & dstFile)
      copyBook = -1
   End Function
End Module
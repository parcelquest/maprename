Option Strict Off
Option Explicit On
Module modMain
	
	Public g_sIndexFile As String
	Public g_sInputDir As String
   Public g_sOutputDir As String
   Public g_sMachine As String
	Public g_sCnty As String
	Public g_asBooks() As String
	Public g_iMaps As Integer
   Public g_iDocNumIdx As Integer
   Public g_iImgNumIdx As Integer
   Public g_bRemove As Boolean

   Public Sub Main()
      Dim sCmd As String
      Dim iRet As Integer
      Dim bProcMap, bProcDoc, bSilent As Boolean

      g_sMachine = System.Environment.MachineName
      g_logFile = Replace(My.Settings.LogDir, "[Machine]", g_sMachine) & "\MapRename_" & Format(Now, "yyyyMMdd") & ".log"
      LogMsg("MapRename " & My.Resources.Version)

      ' Parse command line
      sCmd = ""
      For Each argument As String In My.Application.CommandLineArgs
         If argument = "-M" Then
            bProcMap = True
         ElseIf argument = "-D" Then
            bProcDoc = True
         ElseIf Left(argument, 2) = "-C" Then
            g_sCnty = Mid(argument, 3)
         ElseIf argument = "-R" Then
            g_bRemove = True
         ElseIf argument = "-S" Then
            bSilent = True
         End If
         sCmd = argument
         iRet += 1
      Next

      If sCmd = "?" Or sCmd = "" Then
         Console.WriteLine("MapRename " & My.Resources.Version)
         Console.WriteLine("Usage: MapRename [-C<CntyCode>] [-D] [-R] [-S] ")
         Console.WriteLine("  -C: county code i.e. -CTUO")
         Console.WriteLine("  -D: process docs")
         Console.WriteLine("  -S: silent, no UI")
         Exit Sub
      ElseIf g_sCnty = "" Then
         Console.WriteLine("Usage: MapRename [-C<CntyCode>] [-D] [-R] [-S]")
         Console.WriteLine("***** Missing county.  Please retry with -C<CntyCode> option")
         Exit Sub
      End If

      'Get INI settings
      If Not InitSettings(g_sCnty) Then
         MsgBox("Error initializing MapRename: " & Err.Description)
         Exit Sub
      End If

      If bSilent Then
         If bProcDoc Then
            Call RenameCntyDocs(g_sCnty)
         ElseIf bProcMap Then
            Call RenameCntyMaps(g_sCnty)
         End If
      Else
         frmMapRename.DefInstance.ShowDialog()
      End If
   End Sub
	
   Private Function InitSettings(sCnty As String) As Boolean
      Dim sTmp As String = ""

      InitSettings = True

      Call InitCounty(sCnty)
      Call initCntyInfo(My.Settings.CountyTbl)
      If g_iCounties = 0 Then
         InitSettings = False
      Else
         InitSettings = True
      End If
   End Function

   Public Sub InitCounty(sCnty As String)
      g_sInputDir = Replace(Replace(My.Settings.InputDir, "[Machine]", g_sMachine), "[CO3]", g_sCnty)
      g_sOutputDir = Replace(Replace(My.Settings.OutputDir, "[Machine]", g_sMachine), "[CO3]", g_sCnty)
   End Sub

	Public Sub RenameCntyMaps(ByRef sCnty As String)
		Select Case UCase(sCnty)
			Case "MRN"
				Call renameMRN()
			Case "SBX"
				Call renameSBX()
			Case "SLO"
				Call renameSLO()
			Case Else
				MsgBox("Sorry, this program has not been setup to rename " & sCnty & " maps.")
		End Select
	End Sub
	
	Public Sub RenameCntyDocs(ByRef sCnty As String)	
      Select Case UCase(sCnty)
         Case "TRI"
            g_iImgNumIdx = My.Settings.TRIImgIdx
            g_iDocNumIdx = My.Settings.TRIDocIdx
            Call renameTriDocs(g_sInputDir)

         Case "TUO"
            g_iImgNumIdx = My.Settings.TUOImgIdx
            g_iDocNumIdx = My.Settings.TUODocIdx
            Call renameTuoDocs(g_sInputDir)

         Case Else
            MsgBox("Sorry, this program has not been setup to rename " & sCnty & " docs.")
      End Select
	End Sub
End Module
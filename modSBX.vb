Option Strict Off
Option Explicit On
Module modSBX
	
	Public Sub renameSBX()
		Dim sInpath, sTmp As String
      Dim sOutpath As String
      Dim iCnt, iRet, iBook As Integer
		
		g_iMaps = 0
		
		'Search all input folder
		iBook = 100
		ReDim g_asBooks(iBook)
		
		sInpath = g_sInputDir & "\Book*"
      sTmp = Dir(sInpath, FileAttribute.Directory)
		iCnt = 0
		Do While sTmp <> ""
			g_asBooks(iCnt) = sTmp
			iCnt = iCnt + 1
			If iCnt >= iBook Then
				ReDim Preserve g_asBooks(iBook + 100)
				iBook = iBook + 100
			End If
			
         sTmp = Dir()
		Loop 
		iBook = iCnt
		
		'Loop through all folders
		For iCnt = 0 To iBook - 1
			sTmp = g_asBooks(iCnt)
			sOutpath = g_sOutputDir & "\BK"
			sInpath = g_sInputDir & "\" & sTmp & "\"
			
			'Create subfolders
			Call createSubFolders(sInpath, sOutpath)
			
			'Copy maps
			iRet = copyBooks(sInpath, sOutpath)
			
			System.Windows.Forms.Application.DoEvents()
		Next 
		
		LogMsg("SBX rename is complete with " & g_iMaps & " images")
		Exit Sub
		
renameSBX_Error: 
		LogMsg("Error in renameSBX: " & Err.Description)
	End Sub
	
	Private Sub createSubFolders(ByRef srcPath As String, ByRef dstpath As String)
		Dim dstFile, srcFile, sTmp As String
		'UPGRADE_NOTE: curDir was upgraded to curDir_Renamed. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1061"'
		Dim curDir_Renamed, sFile As String
		Dim sPage As String
		Dim iTmp, iBook, iPage, iCnt As Short
		Dim asBooks(100) As String
		
		On Error GoTo createSubFolders_Error
		
		'Copy the rest
		'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
		srcFile = Dir(srcPath & "*.tif")
		iCnt = 0
		asBooks(0) = ""
		Do While srcFile <> ""
			sTmp = Left(srcFile, 3)
			If sTmp <> asBooks(iCnt) Then
				For iTmp = 1 To iCnt
					If sTmp = asBooks(iTmp) Then Exit For
				Next 
				
				'Create new folder if it doesn't exist
				If iTmp > iCnt Then
					iCnt = iCnt + 1
					asBooks(iCnt) = sTmp
					curDir_Renamed = dstpath & sTmp
					MkDir(curDir_Renamed)
				End If
			End If
			
			'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
			srcFile = Dir()
		Loop 
		
		Exit Sub
		
createSubFolders_Error: 
		LogMsg("Error in createSubFolders: " & Err.Description)
	End Sub
	
	Private Function copyBook(ByRef srcPath As String, ByRef dstpath As String, ByRef sBook As String) As Short
		Dim dstFile, srcFile, sTmp As String
      Dim sFile As String
		Dim sPage As String
		Dim iTmp, iBook, iPage, iCnt As Short
		
		On Error GoTo copyBook_Error
		
      'Copy the rest
      srcFile = Dir(srcPath & "*.tif")
		Do While srcFile <> ""
			iTmp = InStr(2, srcFile, "-")
			If iTmp > 1 Then
				sPage = Mid(srcFile, iTmp + 1, 2)
				dstFile = sBook & sPage & ".00"
			ElseIf UCase(Left(srcFile, 5)) = "INDEX" Then 
				iTmp = InStr(2, srcFile, ".")
				If iTmp > 6 Then
					'Pick up sheet number
					sTmp = Left(srcFile, iTmp - 1)
					iCnt = Val(Mid(sTmp, 6))
				Else
					iCnt = 0
				End If
            dstFile = sBook & "00." & iCnt.ToString("D2") 'VB6.Format(iCnt, "##00")
         Else
            dstFile = "Bad." & iCnt
         End If
			
			FileCopy(srcPath & srcFile, dstpath & dstFile)
			g_iMaps = g_iMaps + 1
         srcFile = Dir()
		Loop 
		
		copyBook = 0
		Exit Function
		
copyBook_Error: 
		LogMsg("Error copying file " & srcFile & " TO " & dstFile)
		copyBook = -1
	End Function
	
	Private Function copyBooks(ByRef srcPath As String, ByRef dstpath As String) As Short
		Dim dstFile, srcFile, sTmp As String
		'UPGRADE_NOTE: curDir was upgraded to curDir_Renamed. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1061"'
		Dim curDir_Renamed, sFile As String
		Dim sBook, sPage As String
		Dim iTmp, iBook, iPage, iCnt As Short
		
		On Error GoTo copyBooks_Error
		
		'Copy the rest
		'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
		srcFile = Dir(srcPath & "*.tif")
		Do While srcFile <> ""
			sBook = Left(srcFile, 3)
			iBook = CShort(sBook)
			iPage = 2
			If iBook = 4 Or iBook = 128 Or iBook > 500 Then
				iPage = 3
			End If
			
			'Check for index map
			If UCase(Mid(srcFile, 4, 1)) = "X" Then
				iTmp = Val(Mid(srcFile, 5))
				dstFile = sBook & New String(Chr(48), iPage) & ".0" & iTmp
			Else
				'Normal maps
				sPage = Mid(srcFile, 4, iPage)
				sTmp = Mid(srcFile, iPage + 4, 1)
				If sTmp <> "." Then
					iTmp = Asc(sTmp) And 15
				Else
					iTmp = 0
				End If
            dstFile = sBook & sPage & "." & iTmp.ToString("D2") 'VB6.Format(iTmp, "##00")
			End If
			
			FileCopy(srcPath & srcFile, dstpath & sBook & "\" & dstFile)
			g_iMaps = g_iMaps + 1
         srcFile = Dir()
		Loop 
		
		copyBooks = 0
		Exit Function
		
copyBooks_Error: 
		LogMsg("Error copying file " & srcFile & " TO " & dstFile)
		copyBooks = -1
	End Function
End Module
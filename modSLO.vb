Option Strict Off
Option Explicit On
Imports System.IO

Module modSLO

   Public Sub renameSLO()
      Dim sInpath, sTmp As String
      Dim sOutpath, sBook As String
      Dim iCnt, iRet, iBook As Short

      'Process county index
      g_iMaps = 0
      sInpath = g_sInputDir & "\Index Maps"
      sTmp = Dir(sInpath, FileAttribute.Directory)
      If sTmp <> "" Then
         sTmp = g_sOutputDir & "\Bk000"
         If 0 = File.Exists(sTmp) Then
            MkDir(sTmp)
         End If
         sOutpath = g_sOutputDir & "\Bk000\00000.00"
         sTmp = sInpath & "\co_index.tif"
         If Dir(sTmp) <> "" Then
            FileCopy(sTmp, sOutpath)
            g_iMaps = g_iMaps + 1
         End If
      End If

      'Search all input folder
      iBook = 100
      ReDim g_asBooks(iBook)

      sInpath = g_sInputDir & "\0*"
      sTmp = Dir(sInpath, FileAttribute.Directory)
      iCnt = 0
      Do While sTmp <> ""
         g_asBooks(iCnt) = sTmp
         iCnt = iCnt + 1
         If iCnt >= iBook Then
            ReDim Preserve g_asBooks(iBook + 100)
            iBook = iBook + 100
         End If

         sTmp = Dir()
      Loop
      iBook = iCnt

      For iCnt = 0 To iBook - 1
         sBook = g_asBooks(iCnt)
         sOutpath = g_sOutputDir & "\Bk" & sBook
         If 0 = File.Exists(sOutpath) Then
            MkDir(sOutpath)
         End If
         sInpath = g_sInputDir & "\" & sBook & "\"
         sOutpath = g_sOutputDir & "\BK" & sBook & "\"
         iRet = copyBook(sInpath, sOutpath, sBook)

         System.Windows.Forms.Application.DoEvents()
      Next

      LogMsg("SLO rename is complete with " & g_iMaps & " images")
      Exit Sub

renameSLO_Error:
      LogMsg("Error in renameSLO: " & Err.Description)
   End Sub

   Private Function copyBook(ByRef srcPath As String, ByRef dstpath As String, ByRef sBook As String) As Short
      Dim dstFile, srcFile, sTmp As String
      Dim iCnt, iTmp, iNdx As Short

      On Error GoTo copyBook_Error

      'Clear list box
      frmMapRename.DefInstance.lstPages.Items.Clear()

      'Copy index maps
      '   iCnt = 0
      '   srcFile = Dir(srcPath & sBook & "ind*.tif")
      '   Do While srcFile <> ""
      '      dstFile = sBook & "000." & Format(iCnt, "##00")
      '      FileCopy srcPath & srcFile, dstpath & dstFile
      '      g_iMaps = g_iMaps + 1
      '      srcFile = Dir()
      '      iCnt = iCnt + 1
      '   Loop

      'Copy subdivision maps
      '   iCnt = 0
      '   srcFile = Dir(srcPath & sBook & "sub*.tif")
      '   Do While srcFile <> ""
      '      dstFile = sBook & "000." & Format(iCnt, "##00")
      '      FileCopy srcPath & srcFile, dstpath & dstFile
      '      g_iMaps = g_iMaps + 1
      '      srcFile = Dir()
      '      iCnt = iCnt + 1
      '   Loop

      'Collect all other tif files in the folder
      srcFile = Dir(srcPath & sBook & "*.tif")
      Do While srcFile <> ""
         frmMapRename.DefInstance.lstPages.Items.Add(srcFile)
         srcFile = Dir()
      Loop

      'Copy regular maps
      iNdx = 0
      For iTmp = 0 To frmMapRename.DefInstance.lstPages.Items.Count - 1
         'srcFile = VB6.GetItemString(frmMapRename.DefInstance.lstPages, iTmp)
         srcFile = frmMapRename.DefInstance.lstPages.GetItemText(iTmp)

         sTmp = UCase(Mid(srcFile, 4, 3))
         If sTmp = "IND" Or sTmp = "SUB" Then
            dstFile = sBook & "000." & iNdx.ToString("D2") 'VB6.Format(iNdx, "##00")
            iNdx = iNdx + 1
         ElseIf Mid(srcFile, 6, 1) = "." Then
            dstFile = Left(srcFile, 5) & ".00"
         ElseIf Mid(srcFile, 7, 1) = "." Then
            dstFile = Left(srcFile, 6) & ".00"
         ElseIf Mid(srcFile, 6, 1) = "-" Or Mid(srcFile, 6, 1) = "_" Then
            iCnt = CShort(Mid(srcFile, 7, 1))
            iCnt = iCnt - 1
            If iCnt >= 0 Then
               dstFile = Left(srcFile, 5) & ".0" & iCnt
            Else
               LogMsg("*** Please verify this file name: " & srcFile)
               dstFile = Left(srcFile, 5) & ".00"
            End If
         ElseIf Mid(srcFile, 7, 1) = "-" Or Mid(srcFile, 7, 1) = "_" Then
            iCnt = CShort(Mid(srcFile, 8, 1))
            iCnt = iCnt - 1
            If iCnt >= 0 Then
               dstFile = Left(srcFile, 6) & ".0" & iCnt
            Else
               LogMsg("*** Please verify this file name: " & srcFile)
               dstFile = Left(srcFile, 6) & ".00"
            End If
         Else
            LogMsg("*** Some thing is wrong here.  Please check: " & srcFile)
            dstFile = ""
         End If

         If dstFile <> "" Then
            FileCopy(srcPath & srcFile, dstpath & dstFile)
            g_iMaps = g_iMaps + 1
         End If
      Next

      copyBook = 0
      Exit Function

copyBook_Error:
      LogMsg("Error copying file " & srcFile & " TO " & dstFile)
      copyBook = -1
   End Function
End Module